angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


    .state('lavapramim', {
    url: '/index',
    templateUrl: 'templates/lavapramim.html',
    controller: 'lavapramimCtrl'
  })

  .state('lavapramimCadastro', {
    url: '/register',
    templateUrl: 'templates/lavapramimCadastro.html',
    controller: 'lavapramimCadastroCtrl'
  })

  .state('lavapramimStop', {
    url: '/stop',
    templateUrl: 'templates/lavapramimStop.html',
    controller: 'lavapramimStopCtrl'
  })

  .state('lavapramimLocation', {
    url: '/location',
    templateUrl: 'templates/lavapramimLocation.html',
    controller: 'lavapramimLocationCtrl'
  })

  .state('lavapramimEdiO', {
    url: '/changeAddress',
    templateUrl: 'templates/lavapramimEdiO.html',
    controller: 'lavapramimEdiOCtrl'
  })

  .state('lavapramimHome', {
    url: '/home',
    templateUrl: 'templates/lavapramimHome.html',
    controller: 'lavapramimHomeCtrl'
  })

  .state('lavapramimConfirmaO', {
    url: '/confirmAddress',
    templateUrl: 'templates/lavapramimConfirmaO.html',
    controller: 'lavapramimConfirmaOCtrl'
  })

  .state('lavapramimListagemDeServiOs', {
    url: '/searchServices',
    templateUrl: 'templates/lavapramimListagemDeServiOs.html',
    controller: 'lavapramimListagemDeServiOsCtrl'
  })

  .state('lavapramimInformaEs', {
    url: '/infoSelectedService',
    templateUrl: 'templates/lavapramimInformaEs.html',
    controller: 'lavapramimInformaEsCtrl'
  })

  .state('lavapramimEntregadores', {
    url: '/deliveryman',
    templateUrl: 'templates/lavapramimEntregadores.html',
    controller: 'lavapramimEntregadoresCtrl'
  })

  .state('lavapramimPagamento', {
    url: '/payment',
    templateUrl: 'templates/lavapramimPagamento.html',
    controller: 'lavapramimPagamentoCtrl'
  })

  .state('lavapramimPagamento2', {
    url: '/listcreditcards',
    templateUrl: 'templates/lavapramimPagamento2.html',
    controller: 'lavapramimPagamento2Ctrl'
  })

  .state('lavapramimPagamento3', {
    url: '/addnewcard',
    templateUrl: 'templates/lavapramimPagamento3.html',
    controller: 'lavapramimPagamento3Ctrl'
  })

  .state('lavapramimSucesso', {
    url: '/success',
    templateUrl: 'templates/lavapramimSucesso.html',
    controller: 'lavapramimSucessoCtrl'
  })
  .state('lavapramimTerms', {
    url: '/terms',
    templateUrl: 'templates/lavapramimTerms.html',
    controller: 'lavapramimTermsCtrl'
  })

$urlRouterProvider.otherwise('/index')


});
