﻿io.sails.url = 'http://localhost:1337';
// Send a GET request to `http://localhost:1337/hello`:
// io.socket.get('/user', function serverResponded (body, JWR) {
//   // body === JWR.body
//   console.log('Sails responded with: ', body);
//   console.log('with headers: ', JWR.headers);
//   console.log('and with status code: ', JWR.statusCode);
//
//   // ...
//   // more stuff
//   // ...
//
//
//   // When you are finished with `io.socket`, or any other sockets you connect manually,
//   // you should make sure and disconnect them, e.g.:
//   io.socket.disconnect();
//
//   // (note that there is no callback argument to the `.disconnect` method)
// });
io.socket.request({
  method: 'get',
  url: '/user',
  data: {
    limit: 15
  },
  headers: {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNDk1NDE3MTg3fQ.dppdylk9Tsw01busZzHeVbulT9I8z12Eie2N3Wi2Ce4'
  }
}, function (resData, jwres) {
  if (jwres.error) {
    console.log(jwres.statusCode); // => e.g. 403
    return;
  }
  console.log(jwres.statusCode); // => e.g. 200
});
// io.socket.post('/login', data, function (resData, JWR){
//   // body === JWR.body
//   console.log('Sails responded with: ', body);
//   console.log('with headers: ', JWR.headers);
//   console.log('and with status code: ', JWR.statusCode);
// });
