const API_URL = 'http://localhost:1337';
angular.module('app.services', [])

.factory('userService', ['$http', UserService])

.service('orderService', ['$http', OrderService]);

function UserService($http){
  const factory = {
    getUsers: () =>
      $http({
        method: 'GET',
        url: API_URL+'/user',
        headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNDk1NDE3MTg3fQ.dppdylk9Tsw01busZzHeVbulT9I8z12Eie2N3Wi2Ce4'}
      }).success(function(data){
        // With the data succesfully returned, call our callback
       // callbackFunc(data);
      }).error(function(err){
        console.log(err)
      }),
    getUserById: (id) =>
      $http({
        method: 'GET',
        url: API_URL+'/user/'+id,
        headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNDk1NDE3MTg3fQ.dppdylk9Tsw01busZzHeVbulT9I8z12Eie2N3Wi2Ce4'}
      }).then(function(response){
        return response;
      }, function(response){
        return(response)
      }),
    createUser: (data) =>
      $http({
        method: 'POST',
        url: API_URL+'/user',
        data: data,
      }).then(function(response){
        console.log(response)
        return response;
      }, function(response){
        console.log(response)
        return(response)
      }),
    saveAddress: (data) =>
      $http({
        method: 'POST',
        url: API_URL+'/useraddress',
        data: data,
      }).then(function(response){
        console.log(response)
        return response;
        }, function(response){
        console.log(response)
        return(response)
        }),


    //   getCurrentUser: (data) =>{
    //   var req = {
    //     method: 'GET',
    //     url: API_URL+'/getCurrentUser',
    //     data: data,
    //     headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNDk2ODk3NDkzfQ.nHL5iEaOJoBuYHury1zrNlAQEcw54JF7FPjuxFMeWlg'}
    //   }
    //   console.log(data)
    // return $http(req).then(function(response){
    //   return response;
    //
    // }, function(response){
    //   return(response)
    // })},
  login: (data) => {
      console.log(data);
      var req = {
        method: 'POST',
        url: API_URL+'/login',
        data: data,
      };
      return $http(req).then(function(response){
        return response;

      }, function(response){
        return(response)
      });
    }
  };
  return factory;

}

function OrderService($http){
  var order = {
    location: null,
    serviceProvider: null,
    numberOfBaskets: null,
    owner: null,
    total: null
  };

  return {
    getOrder: function(){
      return order;
    },
    setOrder: function(data){
      if(data.location)
      order.location = data.location;
      if(data.serviceProvider)
      order.serviceProvider = data.serviceProvider;
      if(data.numberOfBaskets)
      order.numberOfBaskets = data.numberOfBaskets;
      if(data.owner)
        order.owner = data.owner;
      if(data.total)
        order.total = data.total;
      console.log(order)
      return order;
    },
    getLaundries: function(){
      return $http({
        method: 'GET',
        url: API_URL+'/getLaundries',
      }).then(function(response){
        console.log(response)
        return response;
      }, function(response){
        console.log(response)
        return(response)
      });
    },
    getDelivery: function(){
      return $http({
        method: 'GET',
        url: API_URL+'/getDelivery',
      }).then(function(response){
        console.log(response)
        return response;
      }, function(response){
        console.log(response)
        return(response)
      });
    },
    saveOrder: function(){
      return $http({
        method: 'POST',
        url: API_URL+'/order',
        data: order,
      }).then(function(response){
        console.log(response)
        return response;
      }, function(response){
        console.log(response)
        return(response)
      });
    }

  };
}
