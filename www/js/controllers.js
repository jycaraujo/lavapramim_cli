angular.module('app.controllers', ['app.services'])

.controller('lavapramimCtrl', ['$scope', '$stateParams', 'userService', '$state',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, userService, $state) {
  $scope.user = {
    "email": null,
    "password": null
  };
  $scope.login = () => {
    console.log($scope.user)
    userService.login($scope.user).then((data) => {
      if(data.status=='200'){
        $scope.response = data.data;
        localStorage.setItem("token", $scope.response.token);
        localStorage.setItem("currentUserId", $scope.response.user.id);
        $state.go('lavapramimHome');
      }
      else{
          $scope.invalid = true;
      }
    }).catch((err) => {
      console.log("err "+ err)
      $scope.errors = err.data;
      console.log($scope);
    });
  }

  // console.log($scope);
  // userService.getUsers().then(function(users){
  //   console.log(users);
  // });
}])

.controller('lavapramimCadastroCtrl', ['$scope', '$stateParams', 'userService', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, userService, $state) {
  $scope.deliveryP = null;
  $scope.juridical = null;
  $scope.wash = null;
  $scope.washPeople = {
    "companyName": null,
    "cnpj": null,
    "serviceDescription": null,
    "priceByBasket": null,
  };

  $scope.delivery = {
    "description": null,
    "precoKm": null,
  }

  $scope.user = {
    "firstName": null,
    "lastName": null,
    "password": null,
    "confirmPassword": null,
    "cpf": null,
    "userType": null,
  };

  $scope.createUser = () => {
    if($scope.user.userType=='deliverypeople')
      $scope.user.deliveryPeople = $scope.delivery;
    else if($scope.user.userType=='laundry' || $scope.user.userType=='washpeople')
      $scope.user.washPeople = $scope.washPeople;
    console.log($scope.user)
    userService.createUser($scope.user).then((data) => {
      if(data.status=='200'){
        user_id = data.data.user.id;
        $state.go('lavapramimLocation');
      }
    })
  };
  $scope.whichPerson = () => {

    let type = $scope.user.userType;
    console.log(type)
    if(type =='laundry'){
      $scope.deliveryP = false;
      $scope.wash = false;
      $scope.juridical = true;
    }

    else if(type=='deliverypeople'){
      console.log('funciona')
      $scope.deliveryP = true;
      $scope.juridical = false;
      $scope.wash = false;
    }

    else if(type=='washpeople'){
      $scope.wash = true;
      $scope.deliveryP = false;
      $scope.juridical = false;
    }

    else{
      $scope.juridical = false;
      $scope.deliveryP = false;
      $scope.wash = false;
    }
    console.log($scope)
  };

}])

.controller('lavapramimLocationCtrl', ['$scope', '$stateParams', 'userService', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
  function ($scope, $stateParams, userService, $state) {
    $scope.address = {
      "cep": null,
      "address": null,
      "number": null,
      "suburb": null,
      "city": null,
      "user": user_id,
    };

    $scope.saveAddress = () => {
      userService.saveAddress($scope.address).then((data) => {
        if(data.status=='201')
          $state.go('lavapramimSucesso');
      });
    }
  }])

.controller('lavapramimStopCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
  function ($scope, $stateParams) {

  }])


.controller('lavapramimEdiOCtrl', ['$scope', '$stateParams', 'userService', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, userService, $state) {
  $scope.address = {
    "cep": null,
    "address": null,
    "number": null,
    "suburb": null,
    "city": null,
    "user": localStorage.getItem("currentUserId"),
  };

  $scope.saveAddress = () => {
    userService.saveAddress($scope.address).then((data) => {
      if(data.status=='201')
        $state.go('lavapramimSucesso');
  });
  }

}])

.controller('lavapramimHomeCtrl', ['$scope', '$stateParams', 'userService', '$state', '$timeout',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, userService, $state, $timeout) {

  if(localStorage.getItem("token") && localStorage.getItem("currentUserId"))
  {
    $scope.token = localStorage.getItem("token");
    userService.getUserById(localStorage.getItem("currentUserId")).then((data)=>{
      console.log(data);
      $scope.currentUser = data.data;
    }).catch((err)=>{
        console.log(err)
    });
  }
  else{
    $state.go("lavapramimStop")
    $timeout($state.go("lavapramim"), 3000)
  }
}])

.controller('lavapramimConfirmaOCtrl', ['$scope', '$stateParams', 'userService', '$state', 'orderService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, userService, $state, orderService) {
  if(localStorage.getItem("token") && localStorage.getItem("currentUserId")){
      $scope.location = {};
      userService.getUserById(localStorage.getItem("currentUserId")).then((data)=>{
        $scope.currentUser = data.data;
    }).catch((err)=>{
      console.log(err)
    });
    $scope.confirmAddress = () => {

      $scope.orderService = orderService.setOrder({location: $scope.location.id, owner: localStorage.getItem("currentUserId")});
      $state.go("lavapramimListagemDeServiOs")
    }

  }
  else{
    $state.go("lavapramim");
  }
}])

.controller('lavapramimListagemDeServiOsCtrl', ['$scope', '$stateParams', 'orderService', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, orderService, $state) {
  $scope.order = orderService.getOrder();

  $scope.serviceProvider = {};

  orderService.getLaundries().then((laundries) => {
    $scope.laundries = laundries.data.users;
    console.log($scope);
  });

  $scope.setServiceProvider = (id) =>{
    console.log(id);
    $scope.order = orderService.setOrder({serviceProvider: id});
    $state.go("lavapramimInformaEs");
  }

  console.log($scope)

}])

.controller('lavapramimInformaEsCtrl', ['$scope', '$stateParams', 'orderService', 'userService', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, orderService, userService, $state) {
  $scope.order = orderService.getOrder();

  $scope.numberOfBaskets = {};

  $scope.changePrice = () => {
    console.log($scope.order.numberOfBaskets * $scope.laundry.washPeople[0].priceByBasket)
    $scope.order.total = $scope.order.numberOfBaskets * $scope.laundry.washPeople[0].priceByBasket
    $scope.servicePrice = $scope.order.numberOfBaskets * $scope.laundry.washPeople[0].priceByBasket
  }

  $scope.requestOrder = () =>{
    orderService.setOrder($scope.order);
    // orderService.saveOrder();
    $state.go("lavapramimEntregadores");
  }

  userService.getUserById($scope.order.serviceProvider).then((data)=>{
    $scope.laundry = data.data;
    console.log($scope)
  }).catch((err)=>{
      console.log(err)
  });


  // if(localStorage.getItem("token") && localStorage.getItem("currentUserId")){
  //   $scope.location = {};

  //   $scope.confirmAddress = () => {
  //     console.log($scope)
  //     $scope
  //   }
  //
  // }
  // else{
  //   $state.go("lavapramim");
  // }
}])

.controller('lavapramimEntregadoresCtrl', ['$scope', '$stateParams', 'orderService',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, orderService) {
  $scope.order = orderService.getOrder();
  orderService.getDelivery().then((deliveryPeople) => {
    $scope.deliveryPeople = deliveryPeople.data.users;
  console.log($scope);
});


}])

.controller('lavapramimPagamentoCtrl', ['$scope', '$stateParams', 'orderService',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, orderService) {

  $scope.order = orderService.getOrder();
}])

.controller('lavapramimPagamento2Ctrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('lavapramimPagamento3Ctrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('lavapramimSucessoCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
.controller('lavapramimTermsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
  function ($scope, $stateParams) {

  }])
